<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-17
 */

namespace S3p\Html;

use S3p\Html;

/**
 * Class Element
 * @package S3p\Html
 */
abstract class Element {
    /**
     * Element config
     *
     * @var array
     */
    protected $config;

    /**
     * Element supported attributes
     *
     * @example
     * <code>
     *  [
     *    // <name> => <default value> (NULL = no default value)
     *      'type'  => 'text',
     *      'value' => null
     *  ]
     * </code>
     *
     * @var array
     */
    protected $attributes = [];

    /**
     * Child elements container
     * @var array
     */
    protected $elements = [];

    /**
     * Element constructor.
     * @param array $config
     */
    public function __construct(array $config = []) {
        if (array_key_exists('elements', $config)) {
            $this->addElements($config['elements']);
            unset($config['elements']);
        }

        $this->config = $config;
    }

    /**
     * Returns a HTML string
     * @return string
     */
    abstract public function render(): string;

    /**
     * Returns element tag name
     *
     * @return string
     */
    abstract public function getTagName(): string;

    /**
     * Magic method
     *
     * @return string
     */
    public final function __toString(): string {
        return $this->render();
    }

    /**
     * Returns the given config value or if it does not exist then returns the `$default`.
     *
     * @param string $name
     * @param mixed|null $default
     * @return mixed|null
     */
    public final function getConfig(string $name, $default = null) {
        if ($this->hasConfig($name)) {
            $default = $this->config[$name];
        }

        return $default;
    }

    /**
     * Returns TRUE if the given config is existed.
     *
     * @param string $name
     * @return bool
     */
    public final function hasConfig(string $name): bool {
        return array_key_exists($name, $this->config);
    }

    /**
     * Adds child elements
     *
     * @param array $elements
     */
    public final function addElements(array $elements) {
        while (count($elements) > 0) {
            array_push($this->elements, Html::create(array_shift($elements)));
        }
    }

    /**
     * Returns element as a HTML string
     *
     * @return string
     */
    protected final function elementsToString(): string {
        $html = [];

        for ($i = 0; $i < count($this->elements); $i++) {
            array_push($html, $this->elements[$i]->render());
        }

        return implode('', $html);
    }

    /**
     * Returns element attributes
     *
     * @return array
     */
    protected function getAttributes(): array {
        $attributes = [];

        foreach ($this->attributes as $name => $value) {
            $value = $this->getConfig($name, $value);

            if (null !== $value) {
                $attributes[$name] = $value;
            }
        }

        return $attributes;
    }

    /**
     * Returns element attributes as a string
     *
     * @return string
     */
    protected final function attributesToString(): string {
        $attributes     = $this->getAttributes();
        $htmlAttributes = [];

        foreach ($attributes as $name => $value) {
            array_push($htmlAttributes, "$name=\"$value\"");
        }

        return implode(' ', $htmlAttributes);
    }

    /**
     * Returns element id
     *
     * If it is not configured then returns a random id.
     *
     * @return string
     */
    protected function getElementId(): string {
        return $this->getConfig('id', Html::getId());
    }
}
