<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace S3p\Html\Element;

/**
 * Class Number
 * @package S3p\Html\Element
 */
class Number extends Input {
    const NUMBER_TYPE_INTEGER = 'integer';
    const NUMBER_TYPE_FLOAT   = 'float';

    /**
     * @inheritdoc
     * @return array
     */
    protected function getAttributes(): array {
        $attributes         = parent::getAttributes();
        $numberType         = $this->getConfig('numberType', self::NUMBER_TYPE_FLOAT);
        $attributes['type'] = 'number';

        if ($numberType === self::NUMBER_TYPE_INTEGER) {
            $attributes['pattern'] = '^[+-]?[0-9]{0,}$';
        } else {
            $attributes['pattern'] = '^[+-]?[0-9]{0,}(\.[0-9]+)?$';
        };

        return $attributes;
    }
}
