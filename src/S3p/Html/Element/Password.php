<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace S3p\Html\Element;

/**
 * Class Password
 * @package S3p\Html\Element
 */
class Password extends Input {
    /**
     * @inheritdoc
     * @return array
     */
    protected function getAttributes(): array {
        $attributes         = parent::getAttributes();
        $attributes['type'] = 'password';

        return $attributes;
    }
}
