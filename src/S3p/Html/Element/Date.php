<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace S3p\Html\Element;

/**
 * Class Date
 * @package S3p\Html\Element
 */
class Date extends Input {
    /**
     * @inheritdoc
     * @return array
     */
    protected function getAttributes(): array {
        $attributes            = parent::getAttributes();
        $attributes['pattern'] = '^[1-9]?[0-9]{1,3}-[0-1][1-9]-[0-3][0-9]$';
        $attributes['type']    = 'date';

        return $attributes;
    }
}
