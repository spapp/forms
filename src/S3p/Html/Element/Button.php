<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace S3p\Html\Element;

/**
 * Class Button
 * @package S3p\Html\Element
 */
class Button extends Input {
    const DEFAULT_ACTION = 'submit';
    const DEFAULT_TEXT   = 'Submit';

    /**
     * @inheritdoc
     * @return array
     */
    protected function getAttributes(): array {
        $attributes = parent::getAttributes();

        $attributes['type']  = $this->getConfig('action', self::DEFAULT_ACTION);
        $attributes['value'] = $this->getConfig('text', self::DEFAULT_TEXT);

        return $attributes;
    }
}
