<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace S3p\Html\Element;

use S3p\Html\Element;

/**
 * Class Input
 * @package S3p\Html\Element
 */
abstract class Input extends Element {
    /**
     * @inheritdoc
     * @var array
     */
    protected $attributes = [
        'id'                 => null,
        'type'               => null,
        'accept'             => null,
        'accesskey'          => null,
        'autocomplete'       => null,
        'autofocus'          => null,
        'capture'            => null,
        'checked'            => null,
        'disabled'           => null,
        'form'               => null,
        'formaction'         => null,
        'formenctype'        => null,
        'formmethod'         => null,
        'formnovalidate'     => null,
        'formtarget'         => null,
        'height'             => null,
        'inputmode'          => null,
        'list'               => null,
        'max'                => null,
        'maxlength'          => null,
        'min'                => null,
        'minlength'          => null,
        'multiple'           => null,
        'name'               => null,
        'pattern'            => null,
        'placeholder'        => null,
        'readonly'           => null,
        'required'           => null,
        'selectionDirection' => null,
        'selectionEnd'       => null,
        'selectionStart'     => null,
        'size'               => null,
        'spellcheck'         => null,
        'src'                => null,
        'step'               => null,
        'tabindex'           => null,
        'usemap'             => null,
        'value'              => null,
        'width'              => null
    ];

    /**
     * @inheritdoc
     * @return string
     */
    public function getTagName(): string {
        return 'input';
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function render(): string {
        $tagName    = $this->getTagName();
        $attributes = $this->attributesToString();
        $elements   = $this->elementsToString();
        $id         = '';
        $html       = [];
        $label      = $this->getConfig('label');

        if ($label) {
            $id = $this->getElementId();
            array_push($html, '<label for="', $id, '">', $label, '</label>');
        }

        array_push($html, '<', $tagName, ' ', $attributes);

        if ($id) {
            array_push($html, ' id="', $id, '"');
        }

        if ($elements) {
            array_push($html, '>', $elements, '</', $tagName, '>');
        } else {
            array_push($html, '/>');
        }

        return implode('', $html);
    }
}
