<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-17
 */

namespace S3p\Html\Element;

use S3p\Html\Element;

/**
 * Class Form
 * @package S3p\Html\Element
 */
class Form extends Element {
    const ENCTYPE_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
    const ENCTYPE_MULTIPART_FORM_DATA = 'multipart/form-data';
    const ENCTYPE_TEXT_PLAIN          = 'text/plain';

    const AUTOCAPITALIZE_NONE       = 'none';
    const AUTOCAPITALIZE_SENTENCES  = 'sentences';
    const AUTOCAPITALIZE_WORDS      = 'words';
    const AUTOCAPITALIZE_CHARACTERS = 'characters';
    const AUTOCAPITALIZE_ON         = 'on';
    const AUTOCAPITALIZE_OFF        = 'off';

    const AUTOCOMPLETE_ON  = 'on';
    const AUTOCOMPLETE_OFF = 'off';

    const METHOD_POST = 'POST';
    const METHOD_GET  = 'GET';

    const TARGET_SELF   = '_self';
    const TARGET_BLANK  = '_blank';
    const TARGET_PARENT = '_parent';
    const TARGET_TOP    = '_top';

    /**
     * @inheritdoc
     * @var array
     */
    protected $attributes = [
        'accept'         => null,
        'accept-charset' => null,
        'action'         => null,
        'autocapitalize' => null,
        'autocomplete'   => null,
        'enctype'        => self::ENCTYPE_MULTIPART_FORM_DATA,
        'method'         => self::METHOD_POST,
        'name'           => null,
        'novalidate'     => null,
        'target'         => null
    ];

    /**
     * @inheritdoc
     * @return string
     */
    public function getTagName(): string {
        return 'form';
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function render(): string {
        $tagName    = $this->getTagName();
        $attributes = $this->attributesToString();
        $elements   = $this->elementsToString();

        return "<$tagName $attributes>$elements</$tagName>";
    }
}
