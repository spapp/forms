<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace S3p\Html\Element;

use S3p\Html\Element;

/**
 * Class Fieldset
 * @package S3p\Html\Element
 */
class Fieldset extends Element {
    /**
     * @inheritdoc
     * @var array
     */
    protected $attributes = [
        'disabled' => null,
        'form'     => null,
        'name'     => null
    ];

    /**
     * @inheritdoc
     * @return string
     */
    public function getTagName(): string {
        return 'fieldset';
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function render(): string {
        $tagName    = $this->getTagName();
        $attributes = $this->attributesToString();
        $elements   = $this->elementsToString();
        $legend     = $this->getConfig('legend');

        return implode(
            '',
            [
                "<$tagName $attributes>",
                ($legend ? "<legend>$legend</legend>" : ''),
                $elements,
                "</$tagName>"
            ]
        );
    }
}
