<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace S3p;

use S3p\Html\Element;

/**
 * Class Html
 * Factory class
 *
 * @package S3p
 */
class Html {
    /**
     * Namespace
     */
    const NS = 'S3p\\Html\\Element';

    /**
     * ID prefix
     */
    const ID_PREFIX = 's3p-form-';

    private function __construct() {

    }

    /**
     * Factory method
     *
     * Creates an element structure from a config array
     *
     * @param array $config
     * @return Element
     */
    public static function create(array $config): Element {
        $class = self::NS . '\\' . ucfirst($config['type']);

        return new $class($config);
    }

    /**
     * Returns a unique id.
     *
     * @return string
     */
    public static function getId() {
        return uniqid(self::ID_PREFIX);
    }
}
