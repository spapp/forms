<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace S3p;

/**
 * Class Autoloader
 *
 * Class autoloader implementation.
 *
 * @link https://www.php-fig.org/psr/psr-4
 *
 * @example
 * <code>
 *  define('APPLICATION_PATH', dirname(dirname(__FILE__)));
 *
 *  require(APPLICATION_PATH . '/src/S3p/Autoloader.php')
 *
 *  S3p\Autoloader::getInstance()
 *                      ->addIncludePath(APPLICATION_PATH . '/src')
 *                      ->register();
 *
 * </code>
 */
class Autoloader {
    /**
     * PHP file suffix.
     */
    const PHP_FILE_SUFFIX = '.php';

    /**
     * Class instance.
     *
     * @var null|Autoloader
     * @static
     */
    protected static $instance = null;

    /**
     * Include_path configuration option.
     *
     * @var string[]
     */
    protected $includePath = [];

    /**
     * Constructor
     */
    protected function __construct() {
    }

    /**
     * Returns Autoloader instance.
     *
     * @static
     * @return Autoloader
     */
    public static function getInstance(): Autoloader {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Adds some include path option.
     *
     * @param string|array $includePath
     * @return Autoloader
     */
    public function addIncludePath($includePath): Autoloader {
        $this->includePath = array_merge($this->includePath, (array)$includePath);

        return $this;
    }

    /**
     * Returns include path options.
     *
     * @return string[]
     */
    public function getIncludePath(): array {
        return $this->includePath;
    }


    /**
     * Register loader with SPL autoloader stack.
     */
    public function register() {
        spl_autoload_register([$this, 'loadClass']);
    }

    /**
     * Unregister this class loader from the SPL autoloader stack.
     */
    public function unregister() {
        spl_autoload_unregister([$this, 'loadClass']);
    }

    /**
     * Loads the given class or interface.
     *
     * @param string $className The name of the class to load.
     */
    public function loadClass(string $className) {
        $oldIncludePath = get_include_path();

        set_include_path(implode(PATH_SEPARATOR, $this->getIncludePath()) . PATH_SEPARATOR . $oldIncludePath);

        $fileName = str_replace('\\', DIRECTORY_SEPARATOR, rtrim($className, '\\')) . self::PHP_FILE_SUFFIX;

        require_once($fileName);

        set_include_path($oldIncludePath);
    }
}
