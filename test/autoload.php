<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-17
 */

define('APPLICATION_PATH', dirname(dirname(__FILE__)));

require_once APPLICATION_PATH . '/src/S3p/Html.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element/Input.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element/Button.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element/Date.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element/Fieldset.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element/Form.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element/Number.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element/Password.php';
require_once APPLICATION_PATH . '/src/S3p/Html/Element/Text.php';
