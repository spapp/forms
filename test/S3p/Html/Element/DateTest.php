<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace Test\S3p\Html\Element;

use PHPUnit\Framework\TestCase;

use DOMDocument;
use DOMXPath;

use S3p\Html\Element;
use S3p\Html\Element\Date;

class DateTest extends TestCase {
    public function testInstance() {
        $this->assertTrue(new Date() instanceof Element);
    }

    public function testDefaultValue() {
        $domDocument = new DOMDocument();

        $domDocument->loadHTML((new Date())->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//input');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'input');
        $this->assertTrue($entries->item(0)->getAttribute('type') === 'date');

        $pattern = '~' . $entries->item(0)->getAttribute('pattern') . '~';

        $this->assertTrue(preg_match($pattern, '2019-12-11') === 1);
        $this->assertTrue(preg_match($pattern, '1920-02-01') === 1);
        $this->assertTrue(preg_match($pattern, '5920-02-01') === 1);
        $this->assertTrue(preg_match($pattern, '9920-02-01') === 1);
        $this->assertTrue(preg_match($pattern, '920-02-01') === 1);
        $this->assertTrue(preg_match($pattern, '20-02-01') === 1);
        $this->assertTrue(preg_match($pattern, '0920-02-01') === 0);
        $this->assertTrue(preg_match($pattern, '1920-2-1') === 0);
        $this->assertTrue(preg_match($pattern, '4920.02.01') === 0);
        $this->assertTrue(preg_match($pattern, '4920/02/01') === 0);
    }
}