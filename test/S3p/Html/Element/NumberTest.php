<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace Test\S3p\Html\Element;

use PHPUnit\Framework\TestCase;

use DOMDocument;
use DOMXPath;

use S3p\Html\Element;
use S3p\Html\Element\Number;

class NumberTest extends TestCase {
    public function testInstance() {
        $this->assertTrue(new Number() instanceof Element);
    }

    public function testDefaultValue() {
        $domDocument = new DOMDocument();

        $domDocument->loadHTML((new Number())->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//input');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'input');
        $this->assertTrue($entries->item(0)->getAttribute('type') === 'number');

        $pattern = '~' . $entries->item(0)->getAttribute('pattern') . '~';

        $this->assertTrue(preg_match($pattern, '1') === 1);
        $this->assertTrue(preg_match($pattern, '1000') === 1);
        $this->assertTrue(preg_match($pattern, '1234') === 1);
        $this->assertTrue(preg_match($pattern, '+12') === 1);
        $this->assertTrue(preg_match($pattern, '-1234') === 1);

        $this->assertTrue(preg_match($pattern, '1.123') === 1);
        $this->assertTrue(preg_match($pattern, '1000.01') === 1);
        $this->assertTrue(preg_match($pattern, '1234.0') === 1);
        $this->assertTrue(preg_match($pattern, '+12.123') === 1);
        $this->assertTrue(preg_match($pattern, '-1234.00001') === 1);

        $this->assertTrue(preg_match($pattern, '12...2') === 0);
        $this->assertTrue(preg_match($pattern, 'asdf') === 0);
        $this->assertTrue(preg_match($pattern, '4920.02.01') === 0);
        $this->assertTrue(preg_match($pattern, '+-12') === 0);
    }

    public function testInteger() {
        $config      = [
            'numberType' => 'integer'
        ];
        $domDocument = new DOMDocument();

        $domDocument->loadHTML((new Number($config))->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//input');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'input');
        $this->assertTrue($entries->item(0)->getAttribute('type') === 'number');

        $pattern = '~' . $entries->item(0)->getAttribute('pattern') . '~';

        $this->assertTrue(preg_match($pattern, '1') === 1);
        $this->assertTrue(preg_match($pattern, '1000') === 1);
        $this->assertTrue(preg_match($pattern, '1234') === 1);
        $this->assertTrue(preg_match($pattern, '+12') === 1);
        $this->assertTrue(preg_match($pattern, '-1234') === 1);

        $this->assertTrue(preg_match($pattern, '1.123') === 0);
        $this->assertTrue(preg_match($pattern, '1000.01') === 0);
        $this->assertTrue(preg_match($pattern, '1234.0') === 0);
        $this->assertTrue(preg_match($pattern, '+12.123') === 0);
        $this->assertTrue(preg_match($pattern, '-1234.00001') === 0);

        $this->assertTrue(preg_match($pattern, '12...2') === 0);
        $this->assertTrue(preg_match($pattern, 'asdf') === 0);
        $this->assertTrue(preg_match($pattern, '4920.02.01') === 0);
        $this->assertTrue(preg_match($pattern, '+-12') === 0);
    }
}
