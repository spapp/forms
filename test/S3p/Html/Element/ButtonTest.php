<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace Test\S3p\Html\Element;

use PHPUnit\Framework\TestCase;

use DOMDocument;
use DOMXPath;

use S3p\Html\Element;
use S3p\Html\Element\Button;

class ButtonTest extends TestCase {
    public function testInstance() {
        $this->assertTrue(new Button() instanceof Element);
    }

    public function testDefaultValue() {
        $domDocument = new DOMDocument();

        $domDocument->loadHTML((new Button())->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//input');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'input');
        $this->assertTrue($entries->item(0)->getAttribute('type') === 'submit');
        $this->assertTrue($entries->item(0)->getAttribute('value') === 'Submit');
    }

    public function testCustomValue() {
        $onfig       = [
            'action' => 'reset',
            'text'   => 'Cancel'
        ];
        $domDocument = new DOMDocument();
        $button      = new Button($onfig);

        $domDocument->loadHTML($button->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//input');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'input');
        $this->assertTrue($entries->item(0)->getAttribute('type') === $onfig['action']);
        $this->assertTrue($entries->item(0)->getAttribute('value') === $onfig['text']);
    }
}