<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-17
 */

namespace Test\S3p\Html\Element;

use PHPUnit\Framework\TestCase;

use DOMDocument;
use DOMXPath;

use S3p\Html\Element;
use S3p\Html\Element\Form;

class FormTest extends TestCase {
    public function testInstance() {
        $this->assertTrue(new Form() instanceof Element);
    }

    public function testDefaultValue() {
        $domDocument = new DOMDocument();

        $domDocument->loadHTML((new Form())->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//form');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'form');
        $this->assertTrue($entries->item(0)->getAttribute('enctype') === Form::ENCTYPE_MULTIPART_FORM_DATA);
        $this->assertTrue($entries->item(0)->getAttribute('method') === Form::METHOD_POST);
    }

    public function testWithChildren() {
        $domDocument = new DOMDocument();
        $config      = [
            'action'   => 'index.php',
            'elements' => [
                [
                    'type'  => 'date',
                    'label' => 'Date'
                ], [
                    'type' => 'button'
                ]
            ]
        ];

        $domDocument->loadHTML((new Form($config))->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//form');

        $this->assertTrue($entries->length === 1);

        $item = $entries->item(0);
        $this->assertTrue($item->tagName === 'form');
        $this->assertTrue($item->getAttribute('enctype') === Form::ENCTYPE_MULTIPART_FORM_DATA);
        $this->assertTrue($item->getAttribute('method') === Form::METHOD_POST);
        $this->assertTrue($item->getAttribute('action') === $config['action']);
        $this->assertTrue($item->childNodes->length === 3);

        $item = $entries->item(0)->childNodes->item(0);
        $this->assertTrue($item->tagName === 'label');
        $this->assertTrue($item->nodeValue === $config['elements'][0]['label']);

        $item = $entries->item(0)->childNodes->item(1);
        $this->assertTrue($item->tagName === 'input');
        $this->assertTrue($item->getAttribute('type') === $config['elements'][0]['type']);

        $item = $entries->item(0)->childNodes->item(2);
        $this->assertTrue($item->tagName === 'input');
        $this->assertTrue($item->getAttribute('type') === 'submit');
    }
}
