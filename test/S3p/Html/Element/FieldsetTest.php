<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace Test\S3p\Html\Element;

use PHPUnit\Framework\TestCase;

use DOMDocument;
use DOMXPath;

use S3p\Html\Element;
use S3p\Html\Element\Fieldset;

class FieldsetTest extends TestCase {
    public function testInstance() {
        $this->assertTrue(new Fieldset() instanceof Element);
    }

    public function testDefaultValue() {
        $domDocument = new DOMDocument();

        $domDocument->loadHTML((new Fieldset())->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//fieldset');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'fieldset');
    }

    public function testWithLegend() {
        $domDocument = new DOMDocument();
        $config      = [
            'legend' => 'The title'
        ];

        $domDocument->loadHTML((new Fieldset($config))->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//fieldset/legend');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'legend');
        $this->assertTrue($entries->item(0)->nodeValue === $config['legend']);
        $this->assertTrue($entries->item(0)->childNodes->item(0)->nodeType === 3);
        $this->assertTrue($entries->item(0)->childNodes->item(0)->nodeValue === $config['legend']);
    }
}