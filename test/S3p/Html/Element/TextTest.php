<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace Test\S3p\Html\Element;

use PHPUnit\Framework\TestCase;

use DOMDocument;
use DOMXPath;

use S3p\Html\Element;
use S3p\Html\Element\Text;

class TextTest extends TestCase {
    public function testInstance() {
        $this->assertTrue(new Text() instanceof Element);
    }

    public function testDefaultValue() {
        $domDocument = new DOMDocument();

        $domDocument->loadHTML((new Text())->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//input');

        $this->assertTrue($entries->length === 1);
        $this->assertTrue($entries->item(0)->tagName === 'input');
        $this->assertTrue($entries->item(0)->getAttribute('type') === 'text');
    }
}
