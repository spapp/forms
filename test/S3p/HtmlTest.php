<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

namespace Test\S3p;

use PHPUnit\Framework\TestCase;

use DOMDocument;
use DOMXPath;

use S3p\Html;
use S3p\Html\Element\Button;

class HtmlTest extends TestCase {
    private static $config = [
        'type'     => 'form',
        'action'   => 'index.php',
        'method'   => 'POST',
        'elements' => [
            [
                'type'     => 'fieldset',
                'legend'   => 'The Title 1',
                'elements' => [
                    [
                        'type'  => 'text',
                        'name'  => 'username',
                        'label' => 'Username'
                    ]
                ]
            ], [
                'type'     => 'fieldset',
                'legend'   => 'The Title 1',
                'elements' => [
                    [
                        'type'  => 'password',
                        'name'  => 'password',
                        'label' => 'Password'
                    ]
                ]
            ], [
                'type'     => 'fieldset',
                'elements' => [
                    [
                        'type' => 'button',
                        'text' => 'Save'
                    ]
                ]
            ]
        ]
    ];

    public function testGetId() {
        $pattern = '~^' . Html::ID_PREFIX . '~';

        $this->assertTrue(preg_match($pattern, Html::getId()) === 1);
    }

    public function testCreate() {
        $domDocument = new DOMDocument();

        $domDocument->loadHTML(Html::create(self::$config)->render());

        $xpath   = new DOMXPath($domDocument);
        $entries = $xpath->query('//form');

        $this->assertTrue($entries->length === 1);

        $item = $entries->item(0);
        $this->assertTrue($item->tagName === 'form');
        $this->assertTrue($item->getAttribute('method') === self::$config['method']);
        $this->assertTrue($item->getAttribute('action') === self::$config['action']);
        $this->assertTrue($item->childNodes->length === 3);

        // fieldset 0
        $item = $entries->item(0)->childNodes->item(0);
        $this->assertTrue($item->tagName === 'fieldset');
        $this->assertTrue($item->childNodes->length === 3);

        $item = $entries->item(0)->childNodes->item(0)->childNodes->item(0);
        $this->assertTrue($item->tagName === 'legend');
        $this->assertTrue($item->nodeValue === self::$config['elements'][0]['legend']);

        $item = $entries->item(0)->childNodes->item(0)->childNodes->item(1);
        $this->assertTrue($item->tagName === 'label');
        $this->assertTrue($item->nodeValue === self::$config['elements'][0]['elements'][0]['label']);

        $item = $entries->item(0)->childNodes->item(0)->childNodes->item(2);
        $this->assertTrue($item->tagName === 'input');
        $this->assertTrue($item->getAttribute('type') === self::$config['elements'][0]['elements'][0]['type']);
        $this->assertTrue($item->getAttribute('name') === self::$config['elements'][0]['elements'][0]['name']);

        // fieldset 1
        $item = $entries->item(0)->childNodes->item(1);
        $this->assertTrue($item->tagName === 'fieldset');
        $this->assertTrue($item->childNodes->length === 3);

        $item = $entries->item(0)->childNodes->item(1)->childNodes->item(0);
        $this->assertTrue($item->tagName === 'legend');
        $this->assertTrue($item->nodeValue === self::$config['elements'][1]['legend']);

        $item = $entries->item(0)->childNodes->item(1)->childNodes->item(1);
        $this->assertTrue($item->tagName === 'label');
        $this->assertTrue($item->nodeValue === self::$config['elements'][1]['elements'][0]['label']);

        $item = $entries->item(0)->childNodes->item(1)->childNodes->item(2);
        $this->assertTrue($item->tagName === 'input');
        $this->assertTrue($item->getAttribute('type') === self::$config['elements'][1]['elements'][0]['type']);
        $this->assertTrue($item->getAttribute('name') === self::$config['elements'][1]['elements'][0]['name']);

        // fieldset 2
        $item = $entries->item(0)->childNodes->item(2);
        $this->assertTrue($item->tagName === 'fieldset');
        $this->assertTrue($item->childNodes->length === 1);

        $item = $entries->item(0)->childNodes->item(2)->childNodes->item(0);
        $this->assertTrue($item->tagName === 'input');

        $this->assertTrue($item->getAttribute('type') === Button::DEFAULT_ACTION);
        $this->assertTrue($item->getAttribute('value') === self::$config['elements'][2]['elements'][0]['text']);
    }
}