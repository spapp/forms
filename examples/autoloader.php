<?php
/**
 * @author    Sándor Papp <spapp@spappsite.hu>
 * @copyright 2018
 * @license   MIT
 * @since     2018-02-18
 */

define('APPLICATION_PATH', dirname(dirname(__FILE__)));

require_once(APPLICATION_PATH . '/src/S3p/Autoloader.php');

S3p\Autoloader::getInstance()
    ->addIncludePath(APPLICATION_PATH . '/src')
    ->register();