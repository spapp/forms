<?php
namespace S3p\Examples;

use S3p\Html;

require_once(__DIR__.'/../autoloader.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>

    <link href="../style.css" type="text/css" rel="stylesheet">
</head>
<body>

<?php echo Html::create(yaml_parse_file('config.yaml')); ?>

</body>
</html>